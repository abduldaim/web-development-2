<%
    if (session == null || session.getAttribute("is_login") == null || !session.getAttribute("is_login").equals("1")) {
        response.sendRedirect("error.jsp");
    }
%>
<%@ page import="org.eclipse.jetty.demo.LoginUtil" %>
<!DOCTYPE html>
<html>
<head>

    <title>Portal -  Milestone Planner</title>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="style.css" />
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
<div class="container">
    <header class="blog-header py-3">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4 pt-1">
                <a class="blog-header-logo text-dark" ><img width="250px" height="50px"src="./Resources/logo.png"/></a>
            </div>
            <div class="col-4 text-center">



            </div>

            <div class="col-4 d-flex justify-content-end align-items-center">


                <a class="btn btn-sm btn-outline-secondary"  href="../controller/logoutProcess.jsp" value="">Logout</a> &nbsp;

            </div>
        </div>
    </header>

    <h3 align="center"> Welcome <c:out value="${fn:escapeXml(userNameCtx)}"/> to the Dashboard</h3>

    <div id="user_div" >

        <div class="container login-container">
            <div class="row">
                <div class="col-md-6 login-form-1">
                    <h3> About this Application </h3>
                    <br> <br>

                    <div class="row">
                        <div  align="center" class="col-sm-4"> <a href="./viewMilestones.jsp" ><img width="80px" height="80px" src="./Resources/icon1.png"/><p> View Milestones</p></a></div>
                        <div  align="center" class="col-sm-4"> <a href="./AddMileStone.jsp" > <img width="80px" height="80px" src="./Resources/icon2.png"/><p> Add Milestones</p></a></div>
                        <div  align="center" class="col-sm-4"> <a href="./DeleteMilestone.jsp" > <img width="80px" height="80px" src="./Resources/icon3.png"/><p> Delete Milestones</p></a></div>

                    </div>
                    <br>
                    <div>
                        <br>
                        <br>

                    </div>

                    <div class="row">


                        <div  align="center" class="col-sm-4">  <a href="./shareMilestone.jsp" > <img width="80px" height="80px" src="./Resources/icon4.png"/><p> Share Milestone</p></a></div>
                        <div  align="center" class="col-sm-4"> <a href="./membersArea.jsp" > <img width="80px" height="80px" src="./Resources/mem.png"/><p> Member Area</p></a></div>

                        <div  align="center" class="col-sm-4"> <a href="./about.jsp" >   <img width="80px" height="80px" src="./Resources/icon5.png"/><p> About Application</p></a></div>
                    </div>








                </div>
                <div class="col-md-6 login-form-2">
                    <h3>About this Application</h3>
                    <br>
                    <h4 style="color: white;"> This application is built using Apache Maven.
                    <br>
                        In this application, users will need to register an account, using their email address,
                        <br>
                        so that they can then login an access the application. Once a user is at the dashboard,
                        <br>
                        they can choose from a variety of functions to carry out. For example, with it being a
                        <br>
                        milestone planner, they can add a milestone, where they are asked for the milestone name,
                        <br>
                        description, completion date and the name of the person it applies to. Users can view
                        <br>
                        their milestones by clicking on the 'View Milestones' tab.
                        <br>
                        In the event that the milestone has been completed, or no longer applies to the user,
                        <br>
                        it can be deleted from the application by clicking on the 'Delete Milestone' tab, where
                        <br>
                        they can select the milestone they wish to remove from the application.
                        <br>
                        Users can also share their milestones by clicking on the 'Share Milestone' tab, which
                        <br>
                        the application proceeds to generate a link that the user can copy and paste into a message
                        <br>
                        or email that other people can click to view the milestones. This functionality does not grant
                        <br>
                        the people who the link has been shared with the ability to edit the milestones, meaning that
                        <br>
                        only the account owner of the milestone can perform such actions.
                        <br>
                        General Information
                        <br>
                        All the code is stored in the src folder. In it, in the main folder there are 2 additioanl
                        <br>
                        subdirectories. In the java subdirectory is the functionality of the application and in the
                        <br>
                        resources are all the files that are helpers. are all the files that are helpers. The last
                        <br>
                        directory in the src folder is the tests, which stores all the tests for the application.
                        <br>
                        Thanks to the maven structure, we have a target folder, which stores all the pre-built code.
                        <br>
                        To run the application you need to run Main.main() and then you can open the application using
                        <br>
                        http://localhost:8080.
                    </h4>
                </div>
            </div>
        </div>


    </div>






</body>


</html>
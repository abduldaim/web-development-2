# Milestone Planner Application
#
# In this application, users will need to register an account, using their email address, so that they can then login an access the application.
#
# Once a user is at the dashboard, they can choose from a variety of functions to carry out. For example, with it being a milestone planner, they
# can add a milestone, where they are asked for the milestone name, description, completion date and the name of the person it applies to.
#
# Users can view their milestones by clicking on the 'View Milestones' tab.
#
# In the event that the milestone has been completed, or no longer applies to the user, it can be deleted from the application by clicking on the
# 'Delete Milestone' tab, where they can select the milestone they wish to remove from the application.
#
# Users can also share their milestones by clicking on the 'Share Milestone' tab, which the application proceeds to generate a link that the user
# can copy and paste into a message or email that other people can click to view the milestones. This functionality does not grant the people who
# the link has been shared with the ability to edit the milestones, meaning that only the account owner of the milestone can perform such actions.
#
# General Information
# All the code is stored in the src folder. In it, in the main folder there are 2 additioanl subdirectories. In the java subdirectory is the functionality of the application and in the resources are all the files that are helpers.  are all the files that are helpers. The last directory in the src folder is the tests, which stores all the tests for the application. Thanks to the maven structure, we have a target folder, which stores all the pre-built code. 
# 
# To run the application you need to run Main.main() and then you can open the application using http://localhost:8080.